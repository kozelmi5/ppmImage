#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;
class CImage{
public:
    std::string magicNum;
    int width,height,maxcolval;
    size_t size;
    char * pixels;
    friend istream & operator >> (istream &ifstream,  CImage &cImage );
    friend ostream & operator << (ostream &ostream,  CImage &cImage );
    void convertToGreyscale();
     ~CImage(){
      delete [] pixels;
    }
};

 istream & operator >> (istream &ifstream,  CImage &cImage ){
    ifstream >> cImage.magicNum;
    ifstream >> cImage.width;
    ifstream >> cImage.height;
    ifstream >> cImage.maxcolval;
     ifstream.get();
     size_t size = cImage.width * cImage.height * 3;
     cImage.size=size;
     char * rawColours = new char[size];
     ifstream.read(rawColours, size);
     cImage.pixels=rawColours;
    return ifstream;
}
ostream & operator << (ostream &ostream,  CImage &cImage ){
    ostream<<cImage.magicNum<<endl<<cImage.width<<" "<<cImage.height<<endl<<cImage.maxcolval<<endl;
    size_t size = cImage.width * cImage.height * 3;
    ostream.write(cImage.pixels,size);
    return ostream;
}

void CImage::convertToGreyscale() {
    for (size_t i = 0; i <this->size; i=i+3) {
        float fR = pixels[i];
        float fG = pixels[i+1];
        float fB = pixels[i+2];
        float fWB = sqrt((fR * fR + fG * fG + fB * fB) /3);
        pixels[i]=fWB;
        pixels[i+1]=fWB;
        pixels[i+2]=fWB;
    }
}

int main() {

    CImage cImage;
    std::string filename2 = "new.ppm";
    std::string filename = "sign_2.ppm";
    std::ifstream inputstream(filename.c_str(), ifstream::binary);
    std::ofstream out("new.ppm",ofstream::binary);
    if(inputstream.is_open()) {
        cout<<"soubor je otevren"<<endl;
        inputstream>>cImage;
        int a = cImage.width;
        cout<<cImage.magicNum<<" "<<cImage.width<<" "<<cImage.height<<" "<<cImage.maxcolval<<endl;
       cImage.convertToGreyscale();

       out<<cImage;
    }
    return 0;
}
